#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Wrapping of HSSUSB2A windows dll to control Hamamatsu mini-spectrometers.

tested with C11118GA and C11482GA

!! 
!! beware that sleep times are necessary in parts of the code for the stability
!! of the code. They have been tested and reduced to the minimum but they are there. Removing
!! them could result in blockage or crashes of the DLL
!!

"""

import ctypes
import threading
import queue 

import sys
import os
import time
import numpy as np

from enum import Enum

from mosca.Spectrometer import SpectroDataHandler, TriggerMode

# import hamamatsu DLL / check environment variable HAMA_LIB_PATH

default_hama_dir = os.path.join(os.path.dirname(__file__), "hamadll")
hama_dir = os.getenv("HAMAMATSU_LIB_PATH", default_hama_dir)
sys.path.append(hama_dir)

# c.NET imports
import clr
from System import Array, UInt16, Int16, Int64, UInt64, Char, Double
from System.Runtime.InteropServices import GCHandle, GCHandleType
clr.AddReference("HSSUSB2A")

from HSSUSB2_DLL import HSSUSB2, Usb2Struct  # noqa: E402
HAMA_ERRORS = Usb2Struct.Cusb2Err


ACQ_INIT, ACQ_IDLE, ACQ_RUNNING, ACQ_FAULT = (0, 1, 2, 3)
CMD_OK, CMD_NOK = (0,1)

class HamaException(BaseException):
    pass


class HamaCaptureMode(Enum):
    COUNT = 0x00
    CONTINUOUS = 0x01
    TRIGGER = 0x02


class HamaGainMode(Enum):
    Sensor = 0x00
    Circuit = 0x01


class CalibrationCoefficient(Enum):
    Wavelength = 0x00
    Sensitivity = 0x01


class HamaTriggerMode(Enum):
    INTERNAL = 0x00
    SOFTWARE_ASYNC = 0x01
    SOFTWARE_SYNC = 0x02
    EXTERNAL_ASYNC_EDGE = 0x03
    EXTERNAL_ASYNC_LEVEL = 0x04
    EXTERNAL_SYNC_EDGE = 0x05
    EXTERNAL_SYNC_LEVEL = 0x06
    EXTERNAL_SYNC_PULSE = 0x07


class HamaTriggerPolarity(Enum):
    FALLING = 0x01
    RAISING = 0x03


class HamaTriggerOutput(Enum):
    OFF = 0x01
    ON = 0x03


HamaErrors = {
    HAMA_ERRORS.usb2Success: "ok",
    HAMA_ERRORS.usb2Err_unsuccess: "not ok",
    HAMA_ERRORS.usb2Err_notList: "not a list",
    HAMA_ERRORS.usb2Err_notAccess: "cannot access",
    HAMA_ERRORS.usb2Err_notFrame: "not frame",
    HAMA_ERRORS.usb2Err_notAcPower: "not ac power",
    HAMA_ERRORS.usb2Err_notImage: "not image",
    HAMA_ERRORS.usb2Err_notBuffer: "not buffer",
    HAMA_ERRORS.usb2Err_notData: "no data",
    HAMA_ERRORS.usb2Err_notDownload: "not download",
    HAMA_ERRORS.usb2Err_invalidDeviceIndex: "invalid device index",
    HAMA_ERRORS.usb2Err_invalidMode: "invalid mode",
    HAMA_ERRORS.usb2Err_invalidParameter: "invalid parameter",
    HAMA_ERRORS.usb2Err_invalidTriggermode: "invalid trigger mode",
    HAMA_ERRORS.usb2Err_invalidTriggerpolarity: "invalid trigger polarity",
    HAMA_ERRORS.usb2Err_invalidTriggeroutput: "invalid trigger output",
    HAMA_ERRORS.usb2Err_invalidBinning: "invalid binning",
    HAMA_ERRORS.usb2Err_invalidPosition: "invalid position",
    HAMA_ERRORS.usb2Err_invalidSize: "invalid size",
    HAMA_ERRORS.usb2Err_invalidGain: "invalid gain",
    HAMA_ERRORS.usb2Err_invalidOffset: "invalid offset",
    HAMA_ERRORS.usb2Err_invalidCoolingstatus: "invalid cooling status",
    HAMA_ERRORS.usb2Err_invalidFrame: "invalid frame",
    HAMA_ERRORS.usb2Err_invalidEventID: "invalid event Id",
    HAMA_ERRORS.usb2Err_invalidEventhandle: "invalid event handle",
    HAMA_ERRORS.usb2Err_invalidData: "invalid data",
    HAMA_ERRORS.usb2Err_invalidIndex: "invalid index",
    HAMA_ERRORS.usb2Err_invalidNumber: "invalid number",
    HAMA_ERRORS.usb2Err_invalidAddress: "invalid address",
    HAMA_ERRORS.usb2Err_invalidLength: "invalid length",
    HAMA_ERRORS.usb2Err_overTime: "time value too high",
    HAMA_ERRORS.usb2Err_underTime: "time value too low",
    HAMA_ERRORS.usb2Err_overTemperature: "temperature value too high",
    HAMA_ERRORS.usb2Err_underTemperature: "temperature value too low",
    HAMA_ERRORS.usb2Err_overArea: "data out of range of user area",
    HAMA_ERRORS.usb2Err_nowCapturing: "now capturing",
    HAMA_ERRORS.usb2Err_nowFirmUpdate: "updating firmware",
    HAMA_ERRORS.usb2Err_initialized: "already initialized",
    HAMA_ERRORS.usb2Err_opened: "already opened",
    HAMA_ERRORS.usb2Err_existDevice: "device not closed",
    HAMA_ERRORS.usb2Err_FirmCorrupted: "firmware not properly written",
    HAMA_ERRORS.usb2Err_ReadImageStatus: "measurement error",
    HAMA_ERRORS.usb2Err_InvalidEepromParameter: "invalid eeprom parameter",
    HAMA_ERRORS.usb2Err_WaitingStop: "stop in progress",
}


def check_ret(ret):

    isint = isinstance(ret,int)
    interr = isinstance(HAMA_ERRORS.usb2Success,int)

    if isint and not interr:
        if ret == HAMA_ERRORS.usb2Success.value__:
            return True
    elif ret == HAMA_ERRORS.usb2Success:
        return True

    for ky, msg in HamaErrors.items():
        if isint and not interr:
            if ret == ky.value__:
               break
        elif ret == ky:
            break
    else:
        msg = "unknown error code"

    errmsg = f"({ret}) {msg}"
    raise HamaException(errmsg)


_MAP_NET_NP = {
    "Single": np.dtype("float32"),
    "Double": np.dtype("float64"),
    "SByte": np.dtype("int8"),
    "Byte": np.dtype("uint8"),
    "Char": np.dtype("uint8"),
    "Int16": np.dtype("int16"),
    "Int32": np.dtype("int32"),
    "Int64": np.dtype("int64"),
    "UInt16": np.dtype("uint16"),
    "UInt32": np.dtype("uint32"),
    "UInt64": np.dtype("uint64"),
    "Boolean": np.dtype("bool"),
}


def asNumpyArray(netArray):
    """
    Given a CLR `System.Array` returns a `numpy.ndarray`.  See _MAP_NET_NP for
    the mapping of CLR types to Numpy dtypes.
    """
    dims = np.empty(netArray.Rank, dtype=int)
    for idx in range(netArray.Rank):
        dims[idx] = netArray.GetLength(idx)

    netType = netArray.GetType().GetElementType().Name

    try:
        npArray = np.empty(dims, order="C", dtype=_MAP_NET_NP[netType])
    except KeyError:
        raise NotImplementedError(
            "asNumpyArray does not yet support System type {}".format(netType)
        )

    try:  # Memmove
        sourceHandle = GCHandle.Alloc(netArray, GCHandleType.Pinned)
        sourcePtr = sourceHandle.AddrOfPinnedObject().ToInt64()
        destPtr = npArray.__array_interface__["data"][0]
        ctypes.memmove(destPtr, sourcePtr, npArray.nbytes)
    finally:
        if sourceHandle.IsAllocated:
            sourceHandle.Free()

    return npArray


class HamaSpectro:

    MAX_MODULES = 8
    HEADER_SIZE = 256


    def __init__(self, modindex=0):
        super(HamaSpectro, self).__init__()
        self.initialized = False
        self.modules = []

        # commands handled by USBDLL thread
        self.cmds = {
            'connect': self.cmd_connect,
            'disconnect': self.cmd_disconnect,
            'get_status': self.cmd_get_status,
            'get_spectrum_size': self.cmd_get_spectrum_size,
            'get_module_info': self.cmd_get_module_info,
            'get_spectro_info': self.cmd_get_spectro_info,
            'get_capture_mode': self.cmd_get_capture_mode,
            'set_capture_mode': self.cmd_set_capture_mode,
            'allocate_buffer': self.cmd_allocate_buffer,
            'release_buffer': self.cmd_release_buffer,
            'fire_trigger': self.cmd_fire_trigger,
            'get_exposure_time': self.cmd_get_exposure_time,
            'set_exposure_time': self.cmd_set_exposure_time,
            'get_exposure_cycle': self.cmd_get_exposure_cycle,
            'set_exposure_cycle': self.cmd_set_exposure_cycle,
            'get_hama_trigger_mode': self.cmd_get_hama_trigger_mode,
            'set_hama_trigger_mode': self.cmd_set_hama_trigger_mode,
            'get_trigger_polarity': self.cmd_get_trigger_polarity,
            'set_trigger_polarity': self.cmd_set_trigger_polarity,
            'get_trigger_output': self.cmd_get_trigger_output,
            'set_trigger_output': self.cmd_set_trigger_output,
            'get_gain': self.cmd_get_gain,
            'set_gain': self.cmd_set_gain,
            'get_data_count': self.cmd_get_data_count,
            'set_data_count': self.cmd_set_data_count,
            'get_data_transmit': self.cmd_get_data_transmit,
            'set_data_transmit': self.cmd_set_data_transmit,
            'get_data_position': self.cmd_get_data_position,
            'set_data_position': self.cmd_set_data_position,
            'get_trigger_offset': self.cmd_get_trigger_offset,
            'set_trigger_offset': self.cmd_set_trigger_offset,
            'stop_acquisition': self.cmd_stop_acquisition,
            'start_acquisition': self.cmd_start_acquisition,
            'launch_acquisition': self.cmd_launch_acquisition,
            'read_data': self.cmd_read_data,
        }

        self.always_allowed = ['get_status', 'stop_acquisition']

        self.cmdno = 0
        self.cmd_done = [-1,None, None]

        self.info = None
        self.spec_info = None
        self.devindex = None

        self.usbc = None
        self.opened = False

        self.data_handler = SpectroDataHandler()
        self.acq_started = False

        self._acq_frames = 0
        self._stop_it = False

        self._state = ACQ_INIT
        self._spectrum_size = None
        self._launched = False

        self._minimum_cycle_overtime = 2000
        self.exposure_cycle_auto = True
        self._readout_time = 0

        self._model = None
        self._module_info = None
        self._capture_mode = None
        self._hama_trigger_mode = None
        self._data_count = None
        self._data_transmit = None
        self._exposure_time = -1  # 
        self._exposure_time_cmd = -1  # latest cmd value (without readout_time)
        self._exposure_cycle = -1

        # cmd_thread handles ALL access to the DLL
        self.cmd_thread = None

        self._lock = threading.Lock()

        # self.connect()

    def apply_defaults(self):
        #
        # apply defaults through properties
        #
        self.number_frames = 1
        self.buffer_size = 1
        self.capture_mode = HamaCaptureMode.COUNT
        self.gain_mode = HamaGainMode.Sensor
        self.trigger_output = HamaTriggerOutput.OFF
        self.preset_value = 12.0

        # print("GAIN PROP= ", self.get_gain_property(self.gain_mode))
        self.gain = 0
        
        # the following valid only if HamaCaptureMode is Trigger
        self.hama_trigger_mode = HamaTriggerMode.EXTERNAL_SYNC_EDGE
        self.trigger_polarity = HamaTriggerPolarity.RAISING

        self.data_count = 1
        self.data_transmit = 1

    def connect(self, devindex=0):
        self.devindex = devindex
        self.cmd_queue = queue.Queue()

        if not self.initialized:
            ret, retval = self.exec_cmd("connect")
            if ret == 0: 
                self.initialized = True
                self.update_info()
                self.apply_defaults()

    def disconnect(self):
        """
        Release the USB device, the driver and the library.
        To be called before to unplug the device.
        """
        # Release the device. To be called before to unplug the device.
        ret, retval = self.exec_cmd("disconnect")

    def is_opened(self):
        return self.opened

    def get_model(self):
        return self._model

    def get_info(self):
        return self._module_info

    @property
    def gain_mode(self):
        return self._gain_mode

    @gain_mode.setter
    def gain_mode(self, gain_mode):
        self._gain_mode = gain_mode

    @property
    def number_frames(self):
        return self._number_frames

    @number_frames.setter
    def number_frames(self, value):
        self._number_frames = value

    @property
    def acquired_frames(self):
        return self._acq_frames

    @property
    def readout_time(self):
        return self._readout_time

    @readout_time.setter
    def readout_time(self,value):
        self._readout_time = value
        self.set_exposure_time()

    @property
    def trigger_mode(self):
        hama_trig_mode = self.hama_trigger_mode
        capture_mode = self.capture_mode

        if capture_mode == HamaCaptureMode.COUNT:
            return TriggerMode.SOFTWARE
        elif capture_mode == HamaCaptureMode.TRIGGER:
            if hama_trig_mode == HamaTriggerMode.EXTERNAL_SYNC_EDGE:
                return TriggerMode.SYNC
            elif hama_trig_mode == HamaTriggerMode.EXTERNAL_SYNC_LEVEL:
                return TriggerMode.GATE

        # in any other case/combination (not handled by us)
        return TriggerMode.PRIVATE

    @trigger_mode.setter
    def trigger_mode(self, mode):
        if mode == TriggerMode.SOFTWARE:
            self.capture_mode = HamaCaptureMode.COUNT
        elif mode == TriggerMode.SYNC:
            self.capture_mode = HamaCaptureMode.TRIGGER
            self.hama_trigger_mode = HamaTriggerMode.EXTERNAL_SYNC_EDGE
        elif mode == TriggerMode.GATE:
            self.capture_mode = HamaCaptureMode.TRIGGER
            self.hama_trigger_mode = HamaTriggerMode.EXTERNAL_SYNC_LEVEL

    @property
    def capture_mode(self):
        if self._capture_mode is None: 
            if not self.is_acquiring():
                ret, retval = self.exec_cmd("get_capture_mode")
                if ret == CMD_OK:
                    self._capture_mode = retval
            else:
                raise HamaException(f"Cannot execute cmd while acquiring")
        return self._capture_mode

    @capture_mode.setter
    def capture_mode(self, mode):
        if self.is_acquiring():
            raise HamaException(f"Cannot execute cmd while acquiring")

        ret, retval = self.exec_cmd("set_capture_mode", mode)
        if ret == CMD_OK:
            self._capture_mode = mode
        return retval

    @property
    def hama_trigger_mode(self):
        if self._hama_trigger_mode is None:
            if not self.is_acquiring():
                ret, retval = self.exec_cmd("get_hama_trigger_mode")
                if ret == CMD_OK:
                    self._hama_trigger_mode = retval
                    return retval
            else:
                raise HamaException(f"Cannot execute cmd while acquiring")
        return self._hama_trigger_mode 

    @hama_trigger_mode.setter
    def hama_trigger_mode(self, mode):
        if self.is_acquiring():
            raise HamaException(f"Cannot execute cmd while acquiring")

        ret, retval = self.exec_cmd("set_hama_trigger_mode", mode)
        if ret == CMD_OK:
            self._hama_trigger_mode = mode
        return ret

    @property
    def trigger_polarity(self):
        ret, retval = self.exec_cmd("get_trigger_polarity")
        if ret == CMD_OK:
            return retval

    @trigger_polarity.setter
    def trigger_polarity(self, polarity):
        ret, retval = self.exec_cmd("set_trigger_polarity", polarity)
        return retval

    @property
    def trigger_output(self):
        ret, retval = self.exec_cmd("get_trigger_output")
        if ret == CMD_OK:
            return retval

    @trigger_output.setter
    def trigger_output(self, output):
        ret, retval = self.exec_cmd("set_trigger_output", output)
        return retval

    @property
    def gain(self):
        ret, retval = self.exec_cmd("get_gain")
        if ret == CMD_OK:
            return retval

    @gain.setter
    def gain(self, value):
        ret, retval = self.exec_cmd("set_gain", value)
        return retval

    def update_info(self):
        """
        Return info
        """
        if not self.opened:
            raise BaseException("not connected")

        ret, modinfo = self.exec_cmd('get_module_info') 
        ret, specinfo = self.exec_cmd('get_spectro_info') 

        vendor, product, serial, firmware, driver, library = modinfo
        unit, sensor = specinfo

        info_str = f"{vendor} // Model: {product} (serial-no: {serial}) // unit: {unit} // sensor: {sensor} \n"
        info_str += f"firmware={firmware}, driver={driver} lib={library}"

        v = vendor.split(" ")[0]
        m = product.split("-")[0]

        self._model = v + "-" + m
        self._module_info = info_str

        return info_str

    @property
    def minimum_cycle_overtime(self):
        return self._minimum_cycle_overtime

    @minimum_cycle_overtime.setter
    def minimum_cycle_overtime(self, value):
        self._minimum_cycle_overtime = value

    # preset value is exposure time in (milli-secs)
    @property
    def preset_value(self):
        return float(self.exposure_time) / 1000.0

    @preset_value.setter
    def preset_value(self, value):
        self.exposure_time = float(value) * 1000.0

    @property
    def preset_cycle(self):
        return float(self.exposure_cycle / 1000.0)

    @preset_cycle.setter
    def preset_cycle(self, value):
        self.exposure_cycle = float(value) * 1000.0

    # exposure time is the actual Hamamatsu value (in micro-secs)
    @property
    def exposure_time(self):
        if self._exposure_time_cmd != -1:
            return self._exposure_time_cmd
   
        if not self.is_acquiring():
            ret, exptime = self.exec_cmd("get_exposure_time")
            if ret == CMD_OK:
                self._exposure_time = exptime
                return exptime

        return self._exposure_time

    @exposure_time.setter
    def exposure_time(self, exptime):
        self.set_exposure_time(exptime)

    def set_exposure_time(self, exptime=None):
        if exptime is None:
            exptime = self._exposure_time_cmd
        else:
            self._exposure_time_cmd = exptime

        readout_us = self._readout_time * 1000

        #if self.trigger_mode == TriggerMode.SYNC:
        if readout_us != 0 :
            print(f"  / exptime corrected by readout /")
            exptime -= readout_us

        min_cycle = exptime + self.minimum_cycle_overtime

        if min_cycle > self.exposure_cycle:
            ret, retval = self.exec_cmd("set_exposure_cycle", min_cycle)
            cycle_changed = True
        else:
            cycle_changed = False

        ret, retval = self.exec_cmd("set_exposure_time",exptime)

        if ret == CMD_OK:
            self._exposure_time = exptime
            if self.exposure_cycle_auto or cycle_changed:
                ret, retval = self.exec_cmd("set_exposure_cycle", min_cycle)
                if ret == CMD_OK:
                    self._exposure_cycle = min_cycle
            return

    @property
    def exposure_cycle(self):
        if self.is_acquiring():
            return self._exposure_cycle

        ret, value = self.exec_cmd("get_exposure_cycle")
        if ret == CMD_OK:
            self._exposure_cycle = value
            return value

    @exposure_cycle.setter
    def exposure_cycle(self, cycletime):
        if (cycletime + self.minimum_cycle_overtime) < self.exposure_time:
            raise HamaException("cycletime too small for current exposure time")

        # if attribute changed directly (or through preset_cycle)  the auto alignment
        # with exposure_time is broken

        self.exposure_cycle_auto = False
        ret, retval = self.exec_cmd("set_exposure_cycle", cycletime)
        if ret == CMD_OK:
            self._exposure_cycle = cycletime

    @property
    def data_position(self):
        ret, retval = self.exec_cmd("get_data_position")
        if ret == CMD_OK:
            return retval

    @data_position.setter
    def data_position(self, position):
        ret, retval = self.exec_cmd("set_data_position", position)
        return retval

    @property
    def data_trigger_offset(self):
        ret, retval = self.exec_cmd("get_trigger_offset")
        if ret == CMD_OK:
            return retval

    @data_trigger_offset.setter
    def data_trigger_offset(self, offset):
        ret, retval = self.exec_cmd("set_trigger_offset", offset)
        return retval

    @property
    def data_count(self):
        if self._data_count is None:
            ret, retval = self.exec_cmd("get_data_count")
            if ret == CMD_OK:
                self._data_count = retval
        
        return self._data_count

    @data_count.setter
    def data_count(self, count):
        if self._data_count is None or (count != self._data_count):
            ret, retval = self.exec_cmd("set_data_count", count)
            if ret == CMD_OK:
                self._data_count = retval
            return retval

    @property
    def data_transmit(self):
        if self._data_transmit is None:
           ret, retval = self.exec_cmd("get_data_transmit")
           if ret == CMD_OK:
                self._data_transmit = retval

        return self._data_transmit

    @data_transmit.setter
    def data_transmit(self, count):
        if self._data_transmit is None or (count != self._data_transmit):
            ret, retval = self.exec_cmd("set_data_transmit", count)
            if ret == CMD_OK:
                self._data_transmit = retval
            return retval

    @property
    def spectrum_size(self):
        if self._spectrum_size is None:
            ret, xsize = self.exec_cmd("get_spectrum_size")
            self._spectrum_size = xsize
        return self._spectrum_size

    @property
    def buffer_size(self):
        return self._buffer_size

    @buffer_size.setter
    def buffer_size(self, nframes):
        if nframes == 0:
            self.exec_cmd("release_buffer")
        else:
            if nframes <= 10:
                nframes = 10
            self.exec_cmd("allocate_buffer",nframes)
        self._buffer_size = nframes

    def fire_trigger(self):
        self.exec_cmd("fire_trigger")

    def prepare_acquisition(self):
        pass

    def start_acquisition(self):
        self._state = ACQ_RUNNING
        self._acq_frames = 0
        self._stop_it = False

        info = {
            'preset_value': self.preset_value,
            'preset_cycle': self.preset_cycle,
            'preset_real': self.preset_value - self.readout_time,
            'readout_time': self._readout_time,  
            'detector_model': self._model,
        }

        self.data_handler.init_data(self.spectrum_size, data_info=info)

        self.exec_cmd("launch_acquisition")

    def is_acquiring(self):
        return self._state == ACQ_RUNNING

    def is_idle(self):
        return self._state == ACQ_IDLE

    def is_fault(self):
        return self._state == ACQ_FAULT

    def stop_acquisition(self, t1=None):
        if self.is_acquiring():
            self._stop_it = True
            ret, retval = self.exec_cmd("stop_acquisition")

            if ret == CMD_OK:
                self._state = ACQ_IDLE
            else:
                self._state = ACQ_FAULT

    def abort_acquisition(self):
        self.stop_acquisition()
 
    def get_data(self):
        ret, retval = self.exec_cmd("read_data")
        if ret == CMD_OK:
            data, times = retval
            nframes = int(data.shape[0] / self.spectrum_size)
            d = data.reshape(nframes, self.spectrum_size)
        else:
            raise HamaException('Cannot read data')
        return d

    def exec_cmd(self, cmd, cmdpars=None, wait=True):

        if self.cmd_thread is None or (not self.cmd_thread.is_alive()):
            self.cmd_thread = threading.Thread(target=self.cmd_handler, daemon=True)
            self.cmd_thread.start()

        self.cmdno += 1
        cmdno = self.cmdno
        self.cmd_queue.put([self.cmdno,cmd, cmdpars])   

        if wait:
            while True:
                no, ret, retval = self.cmd_done

                if cmdno == no:
                    return ret, retval
                time.sleep(0.005)

    def cmd_handler(self):
        while True:
            try:
                cmdno, cmd, cmdpars = self.cmd_queue.get(timeout=0.05)
                cmd_in_queue = True
            except queue.Empty:
                cmd_in_queue = False
                
            with self._lock:
                if self._launched:
                    self.check_update_acquisition()

                if not cmd_in_queue:
                    continue

                if self._launched:
                    if cmd not in self.always_allowed:
                        print(f"Cannot execute command {cmd} while acquisition is running. Ignored")
                        self.cmd_done = cmdno, CMD_NOK, False
                        continue

                try:
                    if cmd in self.cmds:
                        fn = self.cmds[cmd] 
                        if cmdpars is not None:
                            ret, retval = fn(cmdpars)
                        else:
                            ret, retval = fn()
                        self.cmd_done = cmdno, ret, retval
                    else:
                        print(f'Uhmm. HamaSpectro. trying to exec an unknown queue cmd: {cmd}')
                except BaseException as e:
                    import traceback
                    print(f'exception while executing cmd ({cmd})')
                    traceback.print_exc()

    def cmd_launch_acquisition(self):
        if self._capture_mode == HamaCaptureMode.COUNT:
            nframes = 1
        else:
            nframes = self._number_frames

        print(f' - launching sofware acquisition nframes={nframes}', flush=True)
        self.cmd_release_buffer()
        time.sleep(0.005)
        self.cmd_set_data_count(1)
        self.cmd_set_data_transmit(1)

        buffer_size = (nframes <10) and 10 or nframes
        self.cmd_allocate_buffer(buffer_size)
        self._buffer_size = buffer_size

        self._data_count = 1
        self._data_transmit = 1
        if self._spectrum_size is None:
            ret, self_spectrum_size = self.cmd_get_spectrum_size()

        self.missing_frames = nframes
        self.frame_index = 0

        ret, val = self.cmd_start_acquisition(nframes)
        time.sleep(0.005)

        if ret == CMD_OK:
            self._launched = True
        else:
            print(f'Problem starting acquisition')

        return ret, True

    def check_update_acquisition(self):

        ret, retval = self.cmd_get_status()
        _a, acquired, _b = retval

        if acquired > 0:
            # print(f'checking acquisition got {acquired} frames')
            if self._acq_frames == 0:
                self.time_first = time.perf_counter()

            if self._acq_frames % 5 == 0:
                print(f' - {self._acq_frames}/{self._number_frames} frames acquired')

            # call stop acquisition in SOFTWARE mode
            if self._capture_mode == HamaCaptureMode.COUNT: 
                self.cmd_stop_acquisition()
                time.sleep(0.005)
                # read one by one - first in buffer
                frame_index = 0
                read_nb_frames = 1
            else:  # HamaCaptureMode.TRIGGER
                frame_index = self.frame_index
                if self.missing_frames - acquired <= 0:
                    # we got too many frames
                    read_nb_frames = self.missing_frames
                else:
                    read_nb_frames = acquired

            # read data
            time.sleep(0.005)
            ret, retval = self.cmd_read_data(pars=[frame_index, read_nb_frames])
            data, times = retval
            data = data.reshape(read_nb_frames, self._spectrum_size)
            self.missing_frames -= acquired
            self.frame_index += read_nb_frames

            self.data_handler.add_spectra(data)
            self._acq_frames += read_nb_frames

            # trigger next acquisition in SOFTWARE mode
            if self._capture_mode == HamaCaptureMode.COUNT: 
                if not self._stop_it:
                    if self._acq_frames < self._number_frames:
                        self.cmd_allocate_buffer(10)
                        self.cmd_start_acquisition(1)

        if self._stop_it or (self._acq_frames >= self._number_frames):
            print(f' - {self._acq_frames}/{self._number_frames} frames acquired. Acquisition finished')
            elapsed = (time.perf_counter() - self.time_first) * 1000.0
            if self._number_frames > 1:
                print(f' - elapsed={elapsed:3.0f}ms / {elapsed/(self._number_frames-1):3.0f}ms per frame')


            if self._capture_mode == HamaCaptureMode.TRIGGER: 
                self.cmd_stop_acquisition()

            self._state = ACQ_IDLE
            self._launched = False
            self.data_handler.data_completed()

    def cmd_connect(self):
        self.usbc = HSSUSB2()

        ret = self.usbc.USB2_initialize()
        check_ret(ret)
                
        mlist = Array.CreateInstance(Int16, self.MAX_MODULES)
        ret, no = self.usbc.USB2_getModuleConnectionList(mlist, self.MAX_MODULES)
        check_ret(ret)

        if self.devindex >= no:
            raise ValueError("Selected module index not in module range found")

        ret = self.usbc.USB2_open(self.devindex)
        if not check_ret(ret):
            print("Cannot open device")
            return CMD_NOK, False
        
        self.opened = True
        self._state = ACQ_IDLE

        if self.devindex not in self.modules:
            self.modules.append(self.devindex)

        return CMD_OK, True

    def cmd_disconnect(self):
        if self.usbc is None:
            print("Not connected. Cannot disconnect")
            return CMD_OK, True

        ret = self.usbc.USB2_close(self.devindex)
        if not check_ret(ret):
            print("Cannot close device")
            return CMD_NOK, False
        
        self.opened = False

        self.modules.pop(self.devindex)

        if not self.modules:
            # Release the driver and the library.
            ret = self.usbc.USB2_uninitialize()
            self.initialized = False
            if check_ret(ret):
                print("HamaSpectro Closed")

        return CMD_OK, True

    def cmd_get_spectrum_size(self):
        ret, xsize, ysize = self.usbc.USB2_getImageSize(self.devindex, 0, 0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, xsize

    def cmd_get_module_info(self):
        """
        Return info (vendor, product, serial, firmware, driver, library) got from device.
        """

        modinfo = Usb2Struct.CModuleInformation()
        ret, info = self.usbc.USB2_getModuleInformation(self.devindex, modinfo)

        if not check_ret(ret):
            return CMD_NOK, ret

        vendor = info.vender.split("\x00")[0]
        product = info.product.split("\x00")[0]
        serial = info.serial.split("\x00")[0]
        firmware = info.firmware.split("\x00")[0]
        driver = info.driver.split("\x00")[0]
        library = info.library.split("\x00")[0]

        return CMD_OK, [vendor, product, serial, firmware, driver, library]

    def cmd_get_spectro_info(self):
        spec_info = Usb2Struct.CSpectroInformation()
        ret, info = self.usbc.USB2_getSpectroInformation(self.devindex, spec_info)
        if not check_ret(ret):
            return CMD_NOK, False

        unit = spec_info.unit.split("\x00")[0]
        sensor = spec_info.sensor.split("\x00")[0]
        return CMD_OK, [unit, sensor]

    def cmd_get_capture_mode(self):
        ret, stat = self.usbc.USB2_getCaptureMode(self.devindex, 0)
        if not check_ret(ret):
            return CMD_NOK, False

        self._capture_mode = HamaCaptureMode(stat)
        return CMD_OK, self._capture_mode

    def cmd_set_capture_mode(self, mode):
        ret = self.usbc.USB2_setCaptureMode(self.devindex, mode.value)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_get_status(self):
        ret = self.usbc.USB2_getCaptureStatus(self.devindex, 0, 0)
        return CMD_OK, ret

    def cmd_allocate_buffer(self, nbframes):
        ret = self.usbc.USB2_allocateBuffer(self.devindex, nbframes)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_release_buffer(self):
        ret = self.usbc.USB2_releaseBuffer(self.devindex)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_fire_trigger(self):
        ret = self.usbc.USB2_fireTrigger(self.devindex)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_get_exposure_time(self):
        ret, value = self.usbc.USB2_getExposureTime(self.devindex,0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, value

    def cmd_set_exposure_time(self, exptime):
        ret = self.usbc.USB2_setExposureTime(self.devindex, exptime)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_get_exposure_cycle(self):
        ret, value = self.usbc.USB2_getExposureCycle(self.devindex,0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, value

    def cmd_set_exposure_cycle(self, cycletime):
        ret = self.usbc.USB2_setExposureCycle(self.devindex, cycletime)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_get_hama_trigger_mode(self):
        ret, stat = self.usbc.USB2_getTriggerMode(self.devindex, 0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, HamaTriggerMode(stat)

    def cmd_set_hama_trigger_mode(self, mode):
        ret = self.usbc.USB2_setTriggerMode(self.devindex, mode.value)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_get_trigger_polarity(self):
        ret, stat = self.usbc.USB2_getTriggerPolarity(self.devindex, 0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, HamaTriggerPolarity(stat)

    def cmd_set_trigger_polarity(self, polarity):
        ret = self.usbc.USB2_setTriggerPolarity(self.devindex, polarity.value)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_get_trigger_output(self):
        ret, stat = self.usbc.USB2_getTriggerOutput(self.devindex, 0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, HamaTriggerOutput(stat)

    def cmd_set_trigger_output(self, output):
        ret = self.usbc.USB2_setTriggerOutput(self.devindex, output.value)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_get_gain(self):
        ret, stat = self.usbc.USB2_getGain(self.devindex, self.gain_mode, 0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, stat

    def cmd_set_gain(self, value):
        ret = self.usbc.USB2_setGain(self.devindex, 0, value)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True 

    def cmd_get_trigger_offset(self):
        ret, stat = self.usbc.USB2_getDataTriggerOffset(self.devindex, 0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, stat

    def cmd_set_trigger_offset(self, offset):
        ret = self.usbc.USB2_setDataTriggerOffset(self.devindex, offset)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_get_data_position(self):
        ret, stat = self.usbc.USB2_getDataPosition(self.devindex, 0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, stat

    def cmd_set_data_position(self, position):
        ret = self.usbc.USB2_setDataPosition(self.devindex, position)
        if check_ret(ret):
            return

    def cmd_get_data_count(self):
        ret, stat = self.usbc.USB2_getDataCount(self.devindex, 0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, stat

    def cmd_set_data_count(self, count):
        ret = self.usbc.USB2_setDataCount(self.devindex, count)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_get_data_transmit(self):
        ret, stat = self.usbc.USB2_getDataTransmit(self.devindex, 0)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, stat

    def cmd_set_data_transmit(self, count):
        ret = self.usbc.USB2_setDataTransmit(self.devindex, count)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_start_acquisition(self, frames):
        ret = self.usbc.USB2_captureStart(self.devindex, frames)
        if not check_ret(ret):
            return CMD_NOK, False
        return CMD_OK, True

    def cmd_stop_acquisition(self):
        ret = self.usbc.USB2_captureStop(self.devindex)
        if not check_ret(ret):
            return CMD_NOK, False
            #ret = self.usbc.USB2_captureStop(self.devindex)
            #if not check_ret(ret):
        
        self.acq_started = False
        return CMD_OK, True

    def cmd_read_data(self, pars=[0,1]):
        """
        Return data and ???
        """
        # defaults should be: old_idx=0, frame_cnt=1):
        old_idx, frame_cnt = pars

        img_size = self._spectrum_size * frame_cnt * self._data_transmit
        imgdata = Array.CreateInstance(UInt16, img_size)
        timearr = Array.CreateInstance(UInt64, frame_cnt * self._data_transmit)

        ret = self.usbc.USB2_getImageData(
            self.devindex, imgdata, old_idx, frame_cnt, timearr
        )
        if not check_ret(ret[0]):
            return CMD_NOK, []

        data = asNumpyArray(imgdata)
        times = asNumpyArray(timearr)
        
        return CMD_OK, [data, times]

    #
    #  DIRECT Access functions - used if class instantiated directly. NOT IN THREAD or via SERVER
    # 
    def get_cooling_status(self):
        unit, sensor = self.get_spectro_info()
        if unit[0:2] != "D1":
            return True

        ret, stat = self.usbc.USB2_GetStatusRequest(self.devindex, "c")
        if check_ret(ret):
            stat = ord(stat)

            if 0x1 and stat:
                print("cooling is stable")
                return True

        return False

    def get_cooling_temperature(self):
        ret, stat = self.usbc.USB2_getCoolingTemperature(self.devindex, 0)
        if check_ret(ret):
            return stat

    def get_ad_offset(self):
        ret, stat = self.usbc.USB2_getAdOffset(self.devindex, 0)
        if check_ret(ret):
            return stat

    def set_ad_offset(self, value):
        ret = self.usbc.USB2_setAdOffset(self.devindex, value)
        if check_ret(ret):
            return

    def get_data_count_property(self):
        """
        Number of acquired data ???
        """
        ret, nb = self.usbc.USB2_getDataCountProperty(self.devindex, 0)
        if check_ret(ret):
            return nb

    def get_data_transmit_property(self):
        ret, nb = self.usbc.USB2_getDataTransmitProperty(self.devindex, 0)
        if check_ret(ret):
            return nb

    def read_user_eeprom(self, address, read_size):
        data_arr = Array.CreateInstance(Char, read_size)
        ret, arr = self.usbc.USB2_readEepromUserArea(
            self.devindex, address, data_arr, read_size
        )
        if check_ret(ret):
            data = asNumpyArray(arr)
            return data
        return None

    def get_module_property(self):
        """
        Return functionality of the module
        Capture modes / Binning Modes / Trigger mode / Trigger polarity
        """
        mode_pty = Usb2Struct.CModuleProperty()
        ret, pty = self.usbc.USB2_getModuleProperty(self.devindex, mode_pty)

        if check_ret(ret):
            mod_property = {
                "pixel": pty.pixel,
                "resolution": pty.resolution,
                "capture_modes": pty.capturemode,
                "binning_modes": pty.binningmode,
                "trigger_modes": pty.triggermode,
                "trig_polarity_values": pty.triggerpolarity,
            }
            return mod_property

        return None

    def get_data_position_property(self):
        """
        Return lower / upper bounds of acquiered data ???
        """
        datapos_pty = Usb2Struct.CDataPositionProperty()
        ret, pty = self.usbc.USB2_getDataPositionProperty(self.devindex, datapos_pty)
        if check_ret(ret):
            datapos_property = {
                "lowerposition": pty.lowerposition,
                "upperposition": pty.upperposition,
            }
            return datapos_property
        return None

    def get_exposure_property(self):
        exposure_pty = Usb2Struct.CExposureProperty()
        ret, pty = self.usbc.USB2_getExposureProperty(self.devindex, exposure_pty)
        if check_ret(ret):
            exposure_property = {
                "lowertime": pty.lowertime,
                "uppertime": pty.uppertime,
                "timestep": pty.timestep,
                "lowercycle": pty.lowercycle,
                "uppercycle": pty.uppercycle,
                "cyclestep": pty.cyclestep,
            }
            return exposure_property

        return None

    def get_trigger_offset_property(self):
        triggeroff_pty = Usb2Struct.CDataTriggerOffsetProperty()
        ret, pty = self.usbc.USB2_getDataTriggerOffsetProperty(self.devindex, triggeroff_pty)
        if check_ret(ret):
            triggeroff_property = {
                "lower": pty.lower,
                "upper": pty.upper,
            }
            return triggeroff_property
        return None

    def get_ad_offset_property(self):
        ad_pty = Usb2Struct.CAdOffsetProperty()
        ret, pty = self.usbc.USB2_getAdOffsetProperty(self.devindex, ad_pty)
        if check_ret(ret):
            ad_property = {
                "lower": pty.lower,
                "upper": pty.upper,
            }
            return ad_property
        return None

    def get_wavelength_property(self):
        wl_pty = Usb2Struct.CWaveLengthProperty()
        ret, pty = self.usbc.USB2_getWaveLengthProperty(self.devindex, wl_pty)
        if check_ret(ret):
            wl_property = {
                "lower": pty.lowerwavelength,
                "upper": pty.upperwavelength,
            }
            return wl_property

        return None

    def get_gain_property(self, gain_mode):
        ret, gain_mode = self.usbc.USB2_getGainProperty(self.devindex, gain_mode, 0)
        if check_ret(ret):
            return HamaGainMode(stat)

    def get_calibration_coefficient(self, coeff=None):
        if coeff is None:
            coeff = CalibrationCoefficient.Wavelength.value
        coeff_arr = Array.CreateInstance(Double, 6)

        ret = self.usbc.USB2_getCalibrationCoefficient(self.devindex, coeff, coeff_arr)

        if check_ret(ret):
            arr = asNumpyArray(coeff_arr)
            return arr

    def set_calibration_coefficient(self, coeff, values):
        return "NOT IMPLEMENTED YET"




if __name__ == "__main__":
    hama = HamaSpectro()
    print(hama.get_info())
